# 3DECS
このプロダクトはG's Academyで作成した卒業制作。

Unity、C#、Watson API、Photonを使用し、キャラクターとケース会話ができるもの。

Watson APIでは以下3つを使用

・「Speech To Text（発生した音声をWatson側でテキストに変換）」

・「Text To Speech（Watson側で用意してあるテキストを音声に変換）」

・「Watson Assistant（会話の流れを設定）」

