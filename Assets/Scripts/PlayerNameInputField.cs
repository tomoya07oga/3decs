﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

// セットアップエラーを防ぐために、RequireComponentメソッドをつける。（極論はあってもなくても良い）
[RequireComponent(typeof(InputField))]
public class PlayerNameInputField : MonoBehaviour
{
    const string playerNamePrefKey = "PlayerName";


    // Start is called before the first frame update
    void Start()
    {
        string defaulName = string.Empty;
        InputField _inputField = this.GetComponent<InputField>();
        if(_inputField != null)
        {
            // playerPrefsはplayer情報を保存するためのメソッド
            if (PlayerPrefs.HasKey(playerNamePrefKey))
            {
                defaulName = PlayerPrefs.GetString(playerNamePrefKey);
                _inputField.text = defaulName;
            }
        }

        PhotonNetwork.NickName = defaulName;
    }


    public void SetPlayerName(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            Debug.Log("error");
            return;
        }
        PhotonNetwork.NickName = value;

        PlayerPrefs.SetString(playerNamePrefKey, value);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
