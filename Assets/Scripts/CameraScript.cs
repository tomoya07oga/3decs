﻿/* 
 * Playerに追従させるカメラをPlayerの中に組み込んだ後に以下コードを記載して
 * CameraにAddComponentする
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject target;
    public float distance;
    public float height;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // ターゲットとなる位置を変数に代入
        var p = target.transform.position;

        // 前方にdistance,上方に1だけ離れた場所にカメラが位置する処理
        // targetからみた方向に合わせてカメラも動いて欲しいので、Vector3.forward->target.transform.forwardに変更する
        transform.position = p + target.transform.forward * distance * -1 + Vector3.up * height;

        // targetの位置よりy方向に1高い位置で実行される
        p.y = 1f;

        // LookAt()メソッドは()の中に指定した引数を中心に捉えるように向きを調整するもの
        transform.LookAt(p);
    }
}
