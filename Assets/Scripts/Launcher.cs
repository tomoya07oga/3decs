﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private byte maxPlayersPerRoom = 1;

    [SerializeField]
    private GameObject ProgressPanel;

    [SerializeField]
    private GameObject ControlPanel;

    string gameversion = "1";

    bool isConnecting;

    void Awake()
    {
        Debug.Log("auto");
        PhotonNetwork.AutomaticallySyncScene = true;
    }


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("start");

        // JoinedRoomにLoadlevelコードを記載すると、Connect()が自動的に呼び出されてしまうため、これを消去する
        //Connect();

        ProgressPanel.SetActive(false);
        ControlPanel.SetActive(true);
    }


    public override void OnConnectedToMaster()
    {
        Debug.Log("master");

        if (isConnecting)
        {
            Debug.Log("master2");
            PhotonNetwork.JoinRandomRoom();
            isConnecting = false;
        }

        //ProgressPanel.SetActive(true);
        //ControlPanel.SetActive(false);

        ////base.OnConnectedToMaster();
        //PhotonNetwork.ConnectUsingSettings();
    }


    public override void OnDisconnected(DisconnectCause cause)
    {
        ProgressPanel.SetActive(true);
        ControlPanel.SetActive(false);

        //base.OnDisconnected(cause);
        Debug.Log("disconnect");

    }


    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        //base.OnJoinRandomFailed(returnCode, message);
        Debug.Log("onjoinedRoom");
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
    }

    public override void OnJoinedRoom()
    {
        //base.OnJoinedRoom();
        Debug.Log("onjoinRoom");

        if(PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            // #Critical: We only load if we are the first player, else we rely on `PhotonNetwork.AutomaticallySyncScene` to sync our instance scene.
            Debug.Log("We load the PlayerTimeline");

            // #Critical
            // Load the Room Level.
            PhotonNetwork.LoadLevel("PlayerTimeline");
        }
    }



    public void Connect()
    {
        ProgressPanel.SetActive(true);
        ControlPanel.SetActive(false);

        if (PhotonNetwork.IsConnected)
        {
            Debug.Log("connect");
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            Debug.Log("unconnect");
            //PhotonNetwork.ConnectUsingSettings();

            isConnecting = PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = gameversion;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
